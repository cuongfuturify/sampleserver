const express = require("express");

const app = express();

const port = 3000;
//https://stackoverflow.com/questions/21560335/having-express-js-or-node-accept-an-http-connection-over-port-443
// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

app.get("/", (req, res) => res.send("Hello world"));
app.post("/", function (req, res) {
    res.send("Got a POST request");
});
app.put("/user", function (req, res) {
    res.send("Got a PUT request at /user");
});
app.delete("/user", function (req, res) {
    res.send("Got a DELETE request at /user");
});
//https://expressjs.com/en/guide/routing.html
// Route path: /users/: userId / books /: bookId
// Request URL: http://localhost:3000/users/34/books/8989
// req.params: { "userId": "34", "bookId": "8989" }
app.get("/users/:userId/books/:bookId", function (req, res) {
    res.send(req.params);
});
// Route path: /flights/: from -: to
// Request URL: http://localhost:3000/flights/LAX-SFO
// req.params: { "from": "LAX", "to": "SFO" }
app.get("/flights/:from-:to", function (req, res) {
    res.json(req.params);
});
// Route path: /user/: userId(\d +)
// Request URL: http://localhost:3000/user/42
// req.params: { "userId": "42" }
app.get("/user/:userId(d+)", function (req, res) {
    res.send(req.params);
});

app
    .route("/book")
    .get(function (req, res) {
        res.send("Get a random book");
    })
    .post(function (req, res) {
        res.send(req.body);
    })
    .put(function (req, res) {
        res.send(req.body);
    });

app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!");
});
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send("Something broke!");
});
//specific server
app.listen(port, "192.168.56.1", () => {
    console.log(`Example app listening on port ${port}!`);
});